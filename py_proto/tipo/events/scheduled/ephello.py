#!/usr/bin/env python3
"""Action-HELLO child class of Schedule"""
import logging
import logger
from py_proto.tipo.epsched import EpSchedule
from py_proto.tipo.events.scheduled import HEADER, HELLO, SCHED_INTERVAL, EP_OPERATION_UNSPECIFIED
from py_proto.tipo.events.scheduled import E_TYPE_SCHED, EP_ACT_HELLO, EP_DIR_REQUEST, E_SCHED
from construct import Container
#from py_proto.epheader import EpHeader


class EpHello(EpSchedule):

    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.sequence = 0
        #self.log.info("in EPHello")

        print(self.enbid)
        #self.ephead = EpHeader(self.enbid, self.pci)

    def build_proto_header(self):
        """Over-ridden method from proto_parent and Epsched"""
        #self.type = self.set_proto_header_type(E_TYPE_SCHED)
        #self.enbid = 1 # to be passed by the user call
        #self.pci = 1 #to be passed by the user call
        #self.log.info("in EpHello format master header")
        self.length = HEADER.sizeof() + E_SCHED.sizeof() + HELLO.sizeof()

        #self.seq = self.sequence
        #self.sequence += 1
        return super().build_proto_header() #returns its child instance with parent  attributes

    def build_schedule_header(self):
        """Over-ridden method from Epsched"""
        self.action = self.set_sched_action(EP_ACT_HELLO)
        return super().build_schedule_header()


    def set_hello_rep(self):
        pass

    def get_hello_rep(self):
        pass

    def set_hello_req(self):
        pass

    def get_hello_req(self):
        pass

    #SINGLE EVENT MESSAGES

    def set_single_hello_req(self):
        pass

    def get_single_hello_req(self):
        pass

    def set_single_hello_rep(self):
        pass

    def get_single_hello_rep(self):
        pass

    #SCHEDULE EVENT MESSAGES
    def build_sched_hello(self):
        HLO_MSG = Container(padding=1)
        HELO_MSG = HELLO.build(HLO_MSG)
        FULL = HELO_MSG
        self.log.info(HLO_MSG)
        return FULL


    def get_sched_hello_req(self):
        pass

    def set_sched_hello_rep(self):
        pass

    def get_sched_hello_rep(self):
        pass
