#!/usr/bin/env python3
from py_proto.tipo.epsingle import EpSingle
import logging
import sys

sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger
from construct import Container
from py_proto.tipo.events.single import HEADER, HELLO, SCHED_INTERVAL, EP_OPERATION_UNSPECIFIED
from py_proto.tipo.events.single import E_TYPE_SCHED, EP_ACT_HELLO, EP_DIR_REQUEST, CAPS_C, EP_DIR_REPLY
from py_proto.tipo.events.single import E_TYPE_SINGLE, EP_DIR_REQUEST, E_SINGLE, EP_ACT_ECAP, CAPS_RESPONSE



class EpEnbCap(EpSingle):
    """The ENode B capabilities class as a child class of EpSingle"""
    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        #self.log.info("in EpEnbCap")
        self.ue_measure = 0#default can be changed
        self.ue_report = 1 #default can be changed
        self.noofcells = 1#by default



    def set_enbcap_ue_measure(self, ue_measure):
        self.ue_measure = ue_measure

    def get_enbcap_ue_measure(self):
        return self.ue_measure

    def set_enbcap_ue_report(self, ue_report):
        self.ue_report = ue_report

    def get_enbcap_ue_report(self):
        return self.ue_report

    def set_enbcap_noofcells(self, noofcells):
        self.noofcells = noofcells

    def get_enbcap_noofcells(self):
        return len(self.cellcaplist)

    def build_proto_header(self):
        """Overridden header from proto_parent"""
        #self.log.info("in EpEnbCap format master header")
        #self.length = HEADER.sizeof() + E_SINGLE.sizeof() + CAPS_RESPONSE.sizeof() + self.get_enbcap_noofcells()*CAPS_C.sizeof()
        #self.log.info(str(self.seq)+" sequence number")
        return super().build_proto_header() #returns its child instance with parent  attributes

    def build_single_header(self):
        self.action = self.set_single_action(EP_ACT_ECAP)
        self.direction = self.set_single_direction(EP_DIR_REPLY)
        #self.log.info("in EpCellCap format event header")
        return super().build_single_header()

    def create_enbcap_message(self):
        """Create enbcap message along with cell capabilties
        if there are cells.
        """
        #self.log.info("in EpEnbCap create")
        self.log.info(self.cellcaplist)
        ecap_msg = Container(flags=Container(ue_measure=self.get_enbcap_ue_measure(),
                                             ue_report=self.get_enbcap_ue_report()),
                             nof_cells=self.get_enbcap_noofcells(),
                             cells=self.cellcaplist)

        ecaps_msg = CAPS_RESPONSE.build(ecap_msg)

        return ecaps_msg
