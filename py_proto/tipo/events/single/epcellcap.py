#!/usr/bin/env python3
from py_proto.tipo.epsingle import EpSingle
import logging
import sys

sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger
from construct import Container
from py_proto.tipo.events.single import HEADER, HELLO, SCHED_INTERVAL, EP_OPERATION_UNSPECIFIED
from py_proto.tipo.events.single import  EP_DIR_REQUEST, CAPS_C, CAPS_RESP_SIZE
from py_proto.tipo.events.single import E_TYPE_SINGLE, EP_DIR_REQUEST, E_SINGLE, EP_ACT_CCAP,CAPS_RESPONSE
from py_proto.tipo.events.single.epenbcap import EpEnbCap


class EpCellCap(EpEnbCap):
    """Cells are made"""
    def __init__(self, **kwargs):
        """Cell parameters set by default unless changed"""
        super().__init__()#pylint:disable=E1004
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        if 'pci' in kwargs:
            self.pci = self.set_cellcap_pci(kwargs.get('pci'))
        else:
            self.pci = self.set_cellcap_pci(1)#default can be changed
        if 'cap' in kwargs:
            self.cap = self.set_cellcap_cap(kwargs.get('cap'))
        else:
            self.cap = self.set_cellcap_cap(0)#default can be changed
        if 'dl_freq' in kwargs:
            self.dl_freq = self.set_cellcap_dl_freq(kwargs.get('dl_freq'))
        else:
            self.dl_freq = self.set_cellcap_dl_freq(1750)#default can be changed
        if 'dl_prbs' in kwargs:
            self.dl_prbs = self.set_cellcap_dl_prbs(kwargs.get('dl_prbs'))
        else:
            self.dl_prbs = self.set_cellcap_dl_prbs(25)#default can be changed
        if 'ul_freq' in kwargs:
            self.ul_freq = self.set_cellcap_ul_freq(kwargs.get('ul_freq'))
        else:
            self.ul_freq = self.set_cellcap_ul_freq(19750)#default can be changed
        if 'ul_prbs' in kwargs:
            self.ul_prbs = self.set_cellcap_ul_prbs(kwargs.get('ul_prbs'))
        else:
            self.ul_prbs = self.set_cellcap_ul_prbs(25)#default can be changed
        if 'band' in kwargs:
            self.band = kwargs.get('band')
        else:
            self.band = 3


        self.cellcaplist = [Container(pci=self.pci, cap=self.cap,
                                      DL_earfcn=self.dl_freq,
                                      DL_prbs=self.dl_prbs,
                                      UL_earfcn=self.ul_freq,
                                      UL_prbs=self.ul_prbs)]

        self.pcilist = [self.pci]#
        self.__cellcap_msg = Container()
        #self.log.info("IN EPCELLCAP "+str(self))
        #self.log.info(self.cellcaplist)
        #self.create_cellcap_message()



    def set_cellcap_pci(self, pci):
        """Set the PCI"""
        self.pci = pci
        return self.pci


    def get_cellcap_pci(self):
        """Get the PCI"""
        return self.pci

    def set_cellcap_cap(self, cap):
        """Set the capabilities"""
        self.cap = cap
        return self.cap

    def get_cellcap_cap(self):
        """get the capabilities"""
        return self.cap

    def set_cellcap_dl_freq(self, dl_freq):
        self.dl_freq = dl_freq
        return self.dl_freq

    def get_cellcap_dl_freq(self):
        return self.dl_freq

    def set_cellcap_ul_freq(self, ul_freq):
        self.ul_freq = ul_freq
        return self.ul_freq

    def get_cellcap_ul_freq(self):
        return self.ul_freq

    def set_cellcap_dl_prbs(self, dl_prbs):
        self.dl_prbs = dl_prbs
        return self.dl_prbs

    def get_cellcap_dl_prbs(self):
        return self.dl_prbs

    def set_cellcap_ul_prbs(self, ul_prbs):
        self.ul_prbs = ul_prbs
        return self.ul_prbs

    def get_cellcap_ul_prbs(self):
        return self.ul_prbs

    def build_proto_header(self):
        """Overridden header from enbcap"""
        #self.log.info("in Epcellcap format master header Overidden")
        self.length = HEADER.sizeof() + E_SINGLE.sizeof()+ CAPS_RESP_SIZE + len(self.cellcaplist)*CAPS_C.sizeof()
        #self.log.info("Length is "+str(self.length))
        return super().build_proto_header()

    def build_single_header(self):
        """Overridden method from the enbcap and epsingle"""
        #self.log.info("in EpCellCap format event header in cellcap")
        return super().build_single_header()


    def create_cellcap_message(self):
        """Create cellcap_message ONLY after assigning the values using
        you the attribute setter/getter above that the cell needs to have
        """

        #self.log.info("in cellcap create message")
        self.__cellcap_msg = Container(pci=self.get_cellcap_pci(), cap=self.get_cellcap_cap(),
                                       DL_earfcn=self.get_cellcap_dl_freq(),
                                       DL_prbs=self.get_cellcap_dl_prbs(),
                                       UL_earfcn=self.get_cellcap_ul_freq(),
                                       UL_prbs=self.get_cellcap_ul_prbs())
        #so that duplicate cells are not made and if the user wants to
        #change the attributes of the existing cell
        self.log.info("PCI="+str(self.pci))
        """if self.__cellcap_msg not in self.cellcaplist:

            if self.get_cellcap_pci() in self.pcilist:
                del self.cellcaplist[self.pcilist.index(self.get_cellcap_pci())]
            #    del self.pcilist[self.pcilist.index(self.get_cellcap_pci())]
            else:
                self.cellcaplist.append(self.__cellcap_msg)
                self.pcilist.append(self.get_cellcap_pci())
        else:
            self.log.info("Cannot duplicate cell")"""


        #self.log.info(self.cellcaplist)
        return self.cellcaplist

    def create_enbcap_message(self):
        """Overridden method from enbcap which create enbcap message
        along with cell capabilties if there are cells
        """
        #self.log.info("in EpEnbCap create in cellcap")
        ecap_msg = Container(flags=Container(ue_measure=self.get_enbcap_ue_measure(),
                                             ue_report=self.get_enbcap_ue_report(),
                                             nof_cells=len(self.pcilist),
                                             cells=self.cellcaplist))

        #self.log.info(self.pcilist)
        return super().create_enbcap_message()

    def remove_cell(self):
        """New functionalities?!"""
        pass
