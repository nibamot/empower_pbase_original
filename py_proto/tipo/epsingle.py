#!/usr/bin/env python3
"""One of 3 child classes of Proto_parent
Single Event parent class"""
import logging
import logger

from py_proto.tipo import PT_VERSION
from py_proto.tipo import HEADER, HELLO, EP_OPERATION_UNSPECIFIED
from py_proto.tipo import E_TYPE_SINGLE, EP_DIR_REQUEST, E_SINGLE
from construct import Container

from py_proto.proto_parent import ProtoParent



class EpSingle(ProtoParent):
    """EpSingle protocol operations"""
    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.INFO)
        #self.log.info("in Epsingle")
        self.action = 0
        self.direction = 0
        self.oper = 0
        self.interval = 0
        self.sequence = 0


    def build_proto_header(self):
        """Over-ridden header method from proto_parent"""
        self.type = self.set_proto_header_type(E_TYPE_SINGLE)# specific values for single events
        #self.enbid = self.set_proto_header_enbid(1)# change the value as the user inputs the enb and pci
        #self.pci = self.set_proto_header_pci(1)
        #self.length will have to separately be set although the only singled event
        #is a HELLO and this shold be individually set.
        #self.seq = self.sequence
        #self.sequence += 1
        return super().build_proto_header()

    def set_single_action(self, lst):
        """Pass the action as part of the event header"""
        self.action = lst
        return self.action

    def get_single_action(self):
        pass

    def set_single_direction(self, lst):
        """Pass the direction as request or response"""
        self.direction = lst
        return self.direction

    def get_single_direction(self):
        """parse the direction from event header"""
        pass

    def set_single_oper(self, lst):
        """Pass the operation number"""
        self.oper = lst
        return self.oper

    def get_single_oper(self):
        pass


    def get_single_sched_interval(self):
        """Single events do not have interval,
        just a get to get 0 as the interval"""
        pass


    def build_single_header(self):
        """Building single Event header"""
        evt_hdr = Container(action=self.action, dir=self.direction,
                            op=EP_OPERATION_UNSPECIFIED)
        evthdr_msg = E_SINGLE.build(evt_hdr)
        self.log.debug(evt_hdr)
        return evthdr_msg
