#!/usr/bin/env python3
"""One of 3 child classes of Proto_parent
Scheduled Events parent class"""
import logging
import logger
from py_proto.tipo import E_TYPE_SCHED, PT_VERSION
from py_proto.tipo import HEADER, HELLO, SCHED_INTERVAL, EP_OPERATION_UNSPECIFIED
from py_proto.tipo import E_TYPE_SCHED, EP_ACT_HELLO, EP_DIR_REQUEST, E_SCHED
from construct import Container

from py_proto.proto_parent import ProtoParent


class EpSchedule(ProtoParent):
    """Scheduled protocol messages"""
    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.INFO)
        #self.log.info("in EpSchedule")
        self.action = 0
        self.direction = 0
        self.oper = 0
        self.interval = 0
        self.sequence = 0



    def build_proto_header(self):
        """Over-ridden method from proto_parent"""
        self.type = self.set_proto_header_type(E_TYPE_SCHED)# specific values for schedule events
        #self.enbid = self.enbid# change the value as the user inputs the enb and pci
        #self.length will have to separately be set although the only scheduled event
        #is a HELLO and this shold be individually set.
        #self.seq = self.sequence
        #self.sequence += 1
        return super().build_proto_header()

    def set_sched_action(self, lst):
        """Pass the action as part of the event header"""
        self.action = lst
        return self.action

    def get_sched_action(self):
        pass

    def set_sched_direction(self, lst):
        """Pass the direction as request or response"""
        self.direction = lst
        return self.direction

    def get_sched_direction(self):
        """parse the direction from event header"""
        pass

    def set_sched_oper(self, lst):
        """Pass the operation number"""
        self.oper = lst
        return self.oper

    def get_sched_oper(self):
        pass

    def set_sched_interval(self, lst):
        """Pass custom interval if needed"""
        self.interval = lst
        return self.interval

    def get_sched_interval(self):
        """Parse the interval in event header"""
        pass



    def build_schedule_header(self):
        """Scheduled Event header"""
        evt_hdr = Container(action=self.action, dir=EP_DIR_REQUEST,
                            op=EP_OPERATION_UNSPECIFIED, interval=SCHED_INTERVAL)
        evthdr_msg = E_SCHED.build(evt_hdr)
        #self.log.debug(evt_hdr)
        return evthdr_msg
