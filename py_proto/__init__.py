#!/usr/bin/env python3

""" Proto Parent needs them. """

from construct import Sequence
from construct import Array
from construct import Struct
from construct import UBInt8
from construct import UBInt16
from construct import UBInt32
from construct import UBInt64
from construct import Bytes
from construct import Range
from construct import BitStruct
from construct import Bit
from construct import Padding


PT_VERSION = 0x01


HEADER = Struct("header",
                UBInt8("type"),
                UBInt8("version"),
                UBInt32("enbid"),
                UBInt16("cellid"),
                UBInt32("modid"),
                UBInt16("length"),
                UBInt32("seq"))
