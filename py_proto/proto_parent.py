#!/usr/bin/env python3
"""The skeleton for all protocols/ New version of header"""
import logging
import logger

from py_proto import HEADER
from py_proto import PT_VERSION
from construct import Container



class ProtoParent:
    """The skeleton for all protocols

    The functions below are either an EPF or an EPP
    where the EPF stands for EMpower protocol format(to send)
    and EPP stands for EMpower protocol parse (received) respectively"""

    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        #self.log.info("in parent")
        self.type = 0
        self.version = 0
        self.enbid = 1
        self.pci = 0
        self.modid = 0
        self.length = 0
        self.seq = 0


    """Parse and format operations/ Setter getter"""#pylint:disable=W0105

    def set_proto_header_type(self, _type):# set_proto_header_type
        """set the message type (schedule, single, trigger)"""
        self.type = _type# placeholders
        return self.type

    def get_proto_header_type(self, hdr):
        """get the type of message"""

        return self.type

    def set_proto_header_version(self):
        """The version as of now remains PT_VERSION"""
        self.version = PT_VERSION
        return self.version

    def get_proto_header_version(self, hdr):

        return self.version

    def set_proto_header_enbid(self, _enbid):
        """Passes the ENode-B id"""
        self.enbid = _enbid
        return self.enbid

    def get_proto_header_enbid(self, hdr):

        return self.enbid

    def set_proto_header_pci(self, _pci):
        """Passes the PCI (Cell identifier)"""
        self.pci = _pci
        return self.pci

    def get_proto_header_pci(self):

        return self.pci

    def set_proto_header_modid(self, _mod):
        """Passes the module id from controller"""
        self.modid = _mod
        return self.modid

    def get_proto_header_modid(self, hdr):

        return self.modid

    def set_proto_header_msg_length(self, _len):
        """Passes the message length (sum of MHdr, Ehdr, Act and data for any message)"""
        self.length = _len
        return self.length

    def get_proto_header_msg_length(self, hdr):

        return self.length

    def set_proto_header_sequence(self, _seq):
        """Pass sequentially added sequence number"""
        self.seq = _seq + 1
        return self.seq

    def get_proto_header_sequence(self, hdr):
        """Parsing sequence number"""
        return self.seq


    def build_proto_header(self):
        """ Creating the master header"""
        #self.log.info("in EP Header formatting")
        master_header = Container(type=self.type,
                                  version=self.version, enbid=self.enbid,
                                  cellid=self.pci, modid=self.modid,
                                  length=self.length, seq=self.seq)
        #self.log.info("This seq number "+ str(self.seq)+" belonging to "+ str(type(self)))
        #self.seq = self.set_proto_header_sequence(self.seq)
        master_headbuilt = HEADER.build(master_header)
        #self.log.info(master_header)
        return master_headbuilt
