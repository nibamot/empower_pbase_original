#!/usr/bin/env python3
import socket
from socket import AF_INET, SOCK_STREAM
import tornado
from tornado.ioloop import IOLoop
from tornado import gen
from tornado.iostream import BaseIOStream
import tornado.iostream
import logging
import sys
import time
import io

sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger

from py_proto import HEADER
from proto_parent import ProtoParent
from tipo.epsched import EpSchedule
from tipo.events.scheduled.ephello import EpHello
from tipo.events.single.epcellcap import EpCellCap

"""@gen.coroutine
def async_sleep(seconds):
    yield gen.Task(IOLoop.instance().add_timeout, time.time() + seconds)"""



def main():
    con_sock = socket.socket(AF_INET, SOCK_STREAM, 0)
    con_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 0)
    stream = tornado.iostream.IOStream(con_sock)
    NET_CONNECTED = 0
    while not NET_CONNECTED:
        #yield async_sleep(1)
        try:
            stream.connect(('localhost', 2210))

            NET_CONNECTED = 1
        except OSError:
            print("Trying to connect. Check if controller is active.")

    #self.log.info("No connection")s
    log = logger.getLogger()
    log.setLevel(logging.INFO)

    hello = EpHello()
    sched = EpSchedule()
    cellcap = EpCellCap()
    """ 'cellcap' this is essentially a enb object in cell form
    cell object which can have multiple cells not limited to 6 atleast
    from the controller side unless there are constraints on the TCP
     socket

    """
    print(EpCellCap.__mro__)
    cellcap.create_cellcap_message()
    cellcap.set_cellcap_pci(10)
    #cellcap.set_cellcap_cap(2)
    #cellcap.set_cellcap_dl_freq(2000)
    cellcap.create_cellcap_message()
    cellcap.set_cellcap_pci(13)
    cellcap.set_cellcap_ul_freq(12)
    cellcap.create_cellcap_message()
    print(cellcap.create_enbcap_message())
    #print(EpHello.__mro__)
    #tornado.ioloop.PeriodicCallback(lambda: sender(stream, hello), 2000).start()
    #tornado.ioloop.PeriodicCallback(lambda: reader(stream), 2000).start()
    #log.debug("created a parent and a child object")
    #tornado.ioloop.IOLoop.instance().add_callback(callback=hel)
    tornado.ioloop.IOLoop.current().start()

def timewaste(msg):
    print(msg)


def reader(stri):

    try:
        data = stri.read_bytes(18, callback=timewaste)#uses the bytes read as an argument in the callback
        print(data)
    except (AssertionError, tornado.iostream.StreamClosedError):
        print("nothing received at all")

def sender(str, hello):
    try:

        full = hello.build_proto_header() + hello.build_schedule_header()+hello.build_sched_hello()
        str.write(full, callback=lambda:reader(str))

    except (socket.error, IOError, tornado.iostream.StreamClosedError):
        print("message Sender: Socket Connection closed")
        NET_CONNECTED = 0
        #main()

if __name__ == "__main__":
    main()
