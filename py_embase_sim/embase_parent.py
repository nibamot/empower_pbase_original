#usr/bin/env python3
"""Instance created in the main for each enb and supports proto and agents """
import logging
from multiprocessing.pool import ThreadPool
import threading
import queue
from tornado.ioloop import IOLoop
import embasepy.logger
from embasepy.py_proto.epheader import EpHeader
from embasepy.py_agent.core import CoreFunctions
from embasepy.py_agent.network import NetworkOps

#WORKERS = ThreadPool(1)

class EmbaseParent:
    """ in embase parent"""
    def __init__(self, enbid, pci, cellspecs, port):

        #pylint:disable=C0325
        self.log = embasepy.logger.getLogger()
        self.log.setLevel(logging.INFO)
        self.enbid = enbid
        self.pci = pci
        self.specs = cellspecs
        self.port = port
        #self.log.info(str(threading.active_count())+" before thread")
        self.log.debug("in embase parent enb")
        #self.hdr = EpHeader(self.enbid, self.pci)
        #CoreFunctions()
        self.network = NetworkOps(self.port)
        self.network.net_sc_hello(self.enbid, self.pci)
        #self.nwthread = threading.Thread(target=self.network.connect_controller, args=[self.port])
        #self.stream = self.network.connect_controller(self.port)
        #self.nwthread.start()
        #self.log.info(threading.active_count())
        #print(self.add_enodeb())
        #self.log.info(self.nwthread.is_alive())
        #self.nwthread.join()
        #self.log.info(str(self.nwthread.is_alive())+" after join")

    def add_enodeb(self):
        """Utility for the user"""
        #self.pcimine=23
        pass
        #return self.pcimine

    def start_enb(self, cls):
        """Utility for the user"""
        pass

    def add_ue(self):
        """Utility for the user"""
        pass

    def ue_attach(self):
        """Utility for the user"""
        pass
