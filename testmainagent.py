#!/usr/bin/env python3
"""Main script to run"""
import logging
#import logging.config

import tornado
from tornado.ioloop import IOLoop
import logger
from py_agent.commImpl.net_tcp.vbs.vbs import VBS
from py_agent.commImpl.net_tcp.vbs.cell import Cell
from py_agent.commhub import CommHub
from py_agent.commImpl.net_tcp.network import NetworkTCP




def main():
    """Main of the script users will develop"""
    log = logger.getLogger()
    log_handler = logging.StreamHandler()
    formatter = logging.Formatter(logging.BASIC_FORMAT)
    log_handler.setFormatter(formatter)
    logging.getLogger().addHandler(log_handler)
    logging.getLogger().setLevel(logging.INFO)

    #create master object of commHuB
    comm_hub = CommHub()

    #create cell for the enbs
    cell1 = Cell()
    cell1.add_cell_specs(pci=109)
    cell1.add_cell_specs(pci=1, dl_freq=1763)
    cell1.add_cell_specs(pci=10, dl_freq=1763, ul_freq=19567)

    cell2 = Cell()
    cell2.add_cell_specs()

    #create enBs and add the cells made to them
    enb = VBS(enbid=2, celllist=cell1.celllist)#, celllist=cell1.celllist
    enb2 = VBS(enbid=1, celllist=cell2.celllist)

    #add the enodeBs to the commhub and start it
    comm_hub.add_node(enb)
    comm_hub.add_node(enb2)
    log.info("Added the EnodeBs")
    comm_hub.start()
    log.info("Operations starting")
    #enb.enbstart()
    #enb2.enbstart()

    #log.info("started an enb")
    #enb.net_sched_perform_hello()
    #enb2.net_sched_perform_hello()

    #log.info(VBS.__mro__)


    #enb.add_cell()
    """ch = CommHub()
    cell = Cell()
    enb = VBS(enbid=1, cells=[cell])
    ch.addNode(enb)
    ch.star()"""
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
