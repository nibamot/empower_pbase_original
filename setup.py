#!/usr/bin/env python3
"""Setup script."""

from distutils.core import setup

setup(name="enb_sim",
      version="1.0",
      description="TEst Runtime",
      author="Test",
      packages=['empower_pbase'])
