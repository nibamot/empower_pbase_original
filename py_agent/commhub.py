#!/usr/bin/env python3
"""Parent class of py_agent"""
import logging
import logger


class CommHub():
    """Master class (pun unintended) of py_agent"""
    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.log.info("in CommHub")
        self.enblist = []


    def connect_controller(self):
        """Can be used by the users to implement their own comm"""
        pass

    def send_message(self):
        """Can be used by the users to implement their own comm to send"""
        pass

    def receive_message(self):
        """Can be used by the users to implement their own comm to receive"""
        pass

    def add_node(self, enb):
        """To add the VBS/EnBs to this master object"""
        if enb not in self.enblist:
            self.enblist.append(enb)
            self.log.info("Added to commhub enb "+str(enb.enbid))

    def start(self):
        """EnbStart and connect to controller and net sched hello. Essentially
        master control
        """
        for enb in self.enblist:
            enb.enbstart()
            enb.net_sched_perform_hello()

        self.log.info("started "+str(len(self.enblist))+" EnobeB's")
