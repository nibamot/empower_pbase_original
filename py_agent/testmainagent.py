#!/usr/bin/env python3
import logging
import sys
import tornado
from tornado.ioloop import IOLoop
import empower_pbase.logger
from commImpl.net_tcp.vbs.vbs import VBS
from commImpl.net_tcp.vbs.cell import Cell
from py_agent.commhub import CommHub
from py_agent.commImpl.net_tcp.network import NetworkTCP

#sys.path.insert(0, '/home/empower-t/empower_pbase/')#for logger


def main():
    log = empower_pbase.logger.getLogger()
    log.setLevel(logging.INFO)
    #ch = CommHub()

    #cell1 = Cell(pci=2, cap=0, dl_freq=1747)
    #log.info("Custom cell")
    #enb = VBS()
    #enb.connect_controller()
    """enb1 = VBS()
    enb1.enbstart()
    enb1.net_sched_perform_hello()"""
    #enb.enbstart()
    #enb.connect_controller()
    log.info("started an enb")
    #enb.net_sched_perform_hello()


    #enb.add_cell()
    """ch = CommHub()
    cell = Cell()
    enb = VBS(enbid=1, cells=[cell])
    ch.addNode(enb)
    ch.star()"""
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
