#usr/bin/env python3
import logging
import tornado
import socket
from tornado.ioloop import IOLoop
import logger
from py_agent.commImpl.net_tcp.network import NetworkTCP
from py_agent.commImpl.net_tcp.schedule import ScheduleEvent
from py_agent.commImpl.net_tcp.single import SingleEvent
from py_agent.commImpl.net_tcp.trigger import TriggerEvent
from py_proto.tipo.events.single.epenbcap import EpEnbCap
from py_proto.tipo.events.single.epcellcap import EpCellCap

class Cell():
    """cell class acting as a data container for now"""
    def __init__(self):

        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.cell = None
        self.cellcreate = None
        self.celllist = []
        self.cellcap = None
        """"self.log.info("in CELL "+str(self.cell))
        self.celllist = self.cell.create_cellcap_message()
        for cell in self.celllist:
            self.cell = cell"""



    def add_cell_specs(self, **kwargs):
        """Method to add all the cells to a single EnB (check if cells with same
        pci can be added)
        """
        self.cellcap = EpCellCap(**kwargs)

        for cell in self.cellcap.cellcaplist:

            if cell not in self.celllist:
                self.celllist.append(cell)
            else:
                self.log.info("Exact cell is already present. not adding this cell")

        self.log.info(self.celllist)
