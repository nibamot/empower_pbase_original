#usr/bin/env python3
"""Creating UEs for the pbase"""
import logging
import socket
import logger
from py_agent.commImpl.net_tcp.network import NetworkTCP
from py_agent.commImpl.net_tcp.schedule import ScheduleEvent
from py_agent.commImpl.net_tcp.single import SingleEvent
from py_agent.commImpl.net_tcp.trigger import TriggerEvent
from py_proto.tipo.events.single.epenbcap import EpEnbCap
from py_proto.tipo.events.single.epcellcap import EpCellCap

class UE():
    """Creating UEs"""

    def __init__(self, **kwargs):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.log.info("Creating UE")
        self.ue_id = None
        self.imsi = None
        self.rnti = None
        self.plmn_id = None
        self.cell = None
        self.tenant = None
        self. rrc_measurements = None
