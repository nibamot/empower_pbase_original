#usr/bin/env python3
import logging
import sys
import tornado
import socket
from tornado.ioloop import IOLoop
from construct import Container
sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger
from py_agent.commImpl.net_tcp.network import NetworkTCP
from py_agent.commImpl.net_tcp.schedule import ScheduleEvent
from py_agent.commImpl.net_tcp.single import SingleEvent
from py_agent.commImpl.net_tcp.trigger import TriggerEvent

class VBS(NetworkTCP, ScheduleEvent, SingleEvent, TriggerEvent):
    """Child class of network used primarily to launch instances of VBSes"""
    def __init__(self, **kwargs):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)

        if 'enbid' in kwargs:
            self.enbid = kwargs.get('enbid')
        else:
            self.enbid = 1

        if 'celllist' in kwargs:
            self.celllist = kwargs.get('celllist')
        else:
            self.celllist = [Container(pci=1, cap=0,
                                       DL_earfcn=1750,
                                       DL_prbs=25,
                                       UL_earfcn=19750,
                                       UL_prbs=25)]

        if 'location_x' in kwargs:
            self.location = {'x':kwargs.get('location_x')}
        else:
            self.location = {'x':0.00}

        if 'location_y' in kwargs:
            self.location = {'y':kwargs.get('location_y')}
        else:
            self.location = {'y':0.00}

    #self.log.info("i AM VBS "+str(self.enbid)+" and these cells belong to me "+str(self.celllist))
        # all the attributes of the enb come in here like id, plmn id


    def enbstart(self):
        """Developer uses this call to connect to the controller"""
        self.log.info("in enbstart")
        return super().connect_controller()

    def send_message(self):
        return super().send_message()

    def receive_message(self):
        return super().receive_message()

    def net_sched_perform_hello(self):
        """Overrides net_connected from network.py
        and that in turn does a periodiccallback to net_sc_hello
        """
        return super().net_connected()

    def add_cell(self):
        """adds cells to the enb long with all cell caps"""

        return super().single_cellcap_rep()

    def check_enbid(self):

        return super().check_enbid()
