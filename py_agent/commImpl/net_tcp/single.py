#!/usr/bin/env python3
import logging
import socket
from socket import AF_INET, SOCK_STREAM
import sys
sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger
from py_agent.commImpl.net_tcp import ADDRESS, VALUE, BUFF_SIZE, NET_CONNECTED, E_TYPE_SINGLE, E_TYPE_SCHED, E_TYPE_TRIG
from py_agent.commhub import CommHub
from py_agent.commImpl.net_tcp import HEADER, E_SINGLE, EP_ACT_INVALID, EP_ACT_ECAP, EP_ACT_CCAP, EP_ACT_HANDOVER
from py_proto.tipo.events.single.epenbcap import EpEnbCap
from py_proto.tipo.events.single.epcellcap import EpCellCap

class SingleEvent():
    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.msg = 0
        #self.log.info("in SingleEvent")
        self.celllist = None
        self.epcellcap = EpCellCap()
        self.enbid = 0
        self.seq = 0




    def single_identify_action(self, event):
        """called from network to identify the single event action"""

        act = event.action
        if act == EP_ACT_INVALID:
            return  "The action is invalid"

        elif act == EP_ACT_ECAP:
            #self.log.info("in single_identify_action for ecap")
            return self.single_enbcap_rep()

        elif act == EP_ACT_CCAP:
            #self.log.info("in single_identify_action for ccap")
            return self.single_cellcap_rep()

        elif act == EP_ACT_HANDOVER:
            return self.single_ho_rep()

        return

    def single_cellcap_req(self):
        pass

    def single_enbcap_req(self):
        pass

    def single_ho_req(self):
        pass


    def single_cellcap_rep(self):
        """EnB response for """
        #self.log.info("Forming EnBCap response")
        self.log.info(self.epcellcap.pci)
        return self.epcellcap.create_cellcap_message()


    def single_enbcap_rep(self):
        #self.log.info("in single enbcap_rep in SingleEvent")
        self.log.info(self.enbid)
        self.epcellcap.enbid = self.enbid
        self.epcellcap.cellcaplist = self.celllist
        self.epcellcap.seq = self.seq
        self.msg = (self.epcellcap.build_proto_header() + self.epcellcap.build_single_header() +
                    self.epcellcap.create_enbcap_message())
        #self.log.info("Message made. success!")
        return self.msg

    def single_ho_rep(self):
        pass
