#usr/bin/env python3
import logging
import time
import tornado
from tornado.ioloop import IOLoop
import sys
sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger
from py_proto.tipo.events.scheduled.ephello import EpHello


class ScheduleEvent():
    def __init__(self):
        super().__init__()
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.msg = b''
        #self.log.info("In ScheduleEvent")
        self.ephello = None
        self.enbid = 0
        self.seq = 0


    def sched_send_msg(self):
        pass


    def sched_perform_send(self):
        pass


    def sched_perform_cell_setup(self):
        pass


    def sched_perform_enb_setup(self):
        pass


    def sched_perform_ho(self):
        pass


    def sched_perform_hello(self):
        """scheduled hello"""
        if self.ephello is None:
            self.ephello = EpHello()
        #logging.info("Forming hello")
        self.ephello.enbid = self.enbid
        self.ephello.seq = self.seq
        #self.log.info("ENB ID is "+str(self.ephello.enbid)+" and the sequence number is "+str(self.seq))
        self.msg = (self.ephello.build_proto_header()+self.ephello.build_schedule_header() +
                    self.ephello.build_sched_hello())
        return self.msg
