#!/usr/bin/env python3
""" All basic pre requisites for agent operations"""
import socket
from socket import AF_INET, SOCK_STREAM
from construct import Sequence
from construct import Array
from construct import Struct
from construct import UBInt8
from construct import UBInt16
from construct import UBInt32
from construct import UBInt64
from construct import Bytes
from construct import Range
from construct import BitStruct
from construct import Bit
from construct import Padding

HEADER = Struct("header",
                UBInt8("type"),
                UBInt8("version"),
                UBInt32("enbid"),
                UBInt16("cellid"),
                UBInt32("modid"),
                UBInt16("length"),
                UBInt32("seq"))

E_SCHED = Struct("e_sched",
                 UBInt8("action"),
                 UBInt8("dir"),
                 UBInt8("op"),
                 UBInt32("interval"))

E_SINGLE = Struct("e_single",
                  UBInt8("action"),
                  UBInt8("dir"),
                  UBInt8("op"))

E_TRIG = Struct("e_trig",
                UBInt8("action"),
                UBInt8("dir"),
                UBInt8("op"))


ADDRESS = 'localhost'
VALUE = 1
BUFF_SIZE = 4096
NET_CONNECTED = 0

E_TYPE_SINGLE = 0x01
E_TYPE_SCHED = 0x02
E_TYPE_TRIG = 0x03

EP_ACT_INVALID = 0
EP_ACT_ECAP = 2
EP_ACT_CCAP = 3
EP_ACT_HANDOVER = 7
