#!/usr/bin/env python3
import logging
import socket
from socket import AF_INET, SOCK_STREAM
import tornado
from tornado.ioloop import IOLoop
from tornado.iostream import BaseIOStream
import tornado.iostream
import sys
sys.path.insert(0, '/home/empower-t/empower_pbase/')
import logger
from py_agent.commImpl.net_tcp import ADDRESS, VALUE, BUFF_SIZE, NET_CONNECTED, E_TYPE_SINGLE, E_TYPE_SCHED, E_TYPE_TRIG
from py_agent.commhub import CommHub
from py_agent.commImpl.net_tcp import HEADER, E_SINGLE
from py_agent.commImpl.net_tcp.schedule import ScheduleEvent

class TriggerEvent:
    def __init__(self):
        self.log = logger.getLogger()
        self.log.setLevel(logging.DEBUG)
        #self.log.info("in TriggerEvent")
        super().__init__()

    def trigger_identify_action(self):
        pass
