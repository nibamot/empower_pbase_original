#!/usr/bin/env python3
"""Network TCP"""
import logging
import socket
from socket import AF_INET, SOCK_STREAM
import tornado
from tornado.ioloop import IOLoop
from tornado.iostream import BaseIOStream
import tornado.iostream
import logger
from py_agent.commImpl.net_tcp import ADDRESS, VALUE, BUFF_SIZE, NET_CONNECTED, E_TYPE_SINGLE, E_TYPE_SCHED, E_TYPE_TRIG
from py_agent.commhub import CommHub
from py_agent.commImpl.net_tcp import HEADER, E_SINGLE
from py_agent.commImpl.net_tcp.schedule import ScheduleEvent
from py_agent.commImpl.net_tcp.single import SingleEvent
from py_agent.commImpl.net_tcp.trigger import TriggerEvent

class NetworkTCP(CommHub):
    """TCP implementation of the CommHub classs"""
    def __init__(self):
        super().__init__()#pylint:disable=E1004
        #self.log = logger.getLogger()
        #self.log.setLevel(logging.DEBUG)
        self.port = 2210
        self.seq = 0 #every time a networkops is started the seq set to 0
        self.__buffer = b''
        self.msg = b''
        self.read = 0
        lead = self
        self.__net_connected = 0

        self.log.info(lead.port)
        self.enbid = 0
        self.single = None
        self.schedule = None
        self.trigger = None
        self.celllist = None
        __con_sock = socket.socket(AF_INET, SOCK_STREAM, 0)
        __con_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, VALUE)
        self.stream = tornado.iostream.IOStream(__con_sock)



    def send_message(self):
        """Method to send messages for scheduled events this is periodically called"""

        try:
            self.stream.write(self.msg)#callback=self._wait
            self.seq = self.seq + 1
        except (socket.error, IOError, tornado.iostream.StreamClosedError, AssertionError):
            self.log.info("message Sender: Socket Connection closed")# pylint: disable=C0325, C0303
            raise
        return super().send_message()#pylint:disable=E1004


    def connect_controller(self):
        """ Connect to the controller on localhost with given port"""
        self.log.debug("Connecting")
        self.log.info(self.enbid)


        while not self.__net_connected:
            try:
                self.stream.connect((ADDRESS, self.port))
                self.log.debug(self.stream)
                self.__net_connected = 1
                self.log.info("Connected")
            except OSError:
                self.log.info("Trying to connect. Check if controller is active.")
                """move this to VBS as we want only network TCP stuff all the other things are to
                be done in VBS"""
        #tornado.ioloop.IOLoop.instance().add_callback(self.net_connected)
        #if not con_sock:#useless
            #self.log.debug("Could not connect to controller")
        self._wait()
        return super().connect_controller()


    def _wait(self):
        """wait after connection"""
        self.__buffer = b''
        self.stream.read_bytes(HEADER.sizeof(), self._on_read)

    def _on_read(self, line):
        "Reads data from the controller in a buffer"
        self.__buffer = self.__buffer + line
        hdr = HEADER.parse(self.__buffer)
        self.log.info(hdr)
        if len(self.__buffer) < hdr.length:
            remaining = hdr.length - len(self.__buffer)
            self.stream.read_bytes(remaining, self._on_read)
            return

        try:
            self.net_process_message(hdr)

        except Exception as ex:
            self.log.info(ex)
            logging.info("in on_read Exception")
            self.stream.close()

        if not self.stream.closed():
            self._wait()



    def net_sched_job(self, hdr):
        """Adds job to the schedule. calls sched_add_job in scheduler """
        self.log.info("The job to be scheduled")
        #event = hdr.type


    def net_connected(self):
        self.log.info("Connected to controller at "+str(socket.gethostbyname(ADDRESS))
                      +" on port "+ str(self.port))
        try:
            self.sender = tornado.ioloop.PeriodicCallback(self.net_sc_hello, 2000)
            self.sender.start()

        except(socket.error, IOError, tornado.iostream.StreamClosedError, AssertionError):
            self.log.info("message Sender: Socket Connection closed from net_connected")
            self.sender.stop()

    def net_sc_hello(self):
        #self.log.info("in net_sc_hello")
        if self.schedule is None:
            self.schedule = ScheduleEvent()
        try:
            self.schedule.enbid = self.enbid
            self.schedule.seq = self.seq
            self.msg = self.schedule.sched_perform_hello()
            logging.info(self.msg)
            self.send_message()
        except(socket.error, IOError, tornado.iostream.StreamClosedError, AssertionError):
            self.log.info("message Sender: Socket Connection closed from net_sc_hello")
            self.disconnect_controller()
            self.sender.stop()
        #self.sender = tornado.ioloop.PeriodicCallback(self.send_message, 2000)
        #self.sender.start()

    def net_se_cell_setup(self):
        pass

    def net_se_enb_setup(self):
        pass

    def net_se_ho(self):
        pass

    def net_se_rans(self):
        pass

    def net_se_rant(self):
        pass

    def net_se_ranu(self):
        pass

    def net_se_ranc(self):
        pass

    def net_te_ue_measure(self):
        pass

    def net_te_ue_report(self):
        pass

    def net_te_mac_report(self):
        pass

    def net_process_sched_event(self):
        """Process scheduled events"""
        if self.schedule is None:
            self.schedule = ScheduleEvent()

    def net_process_single_event(self, event):
        """Process single events"""
        if self.single is None:
            self.single = SingleEvent()
        try:
            self.single.celllist = self.celllist
            self.single.enbid = self.enbid
            self.single.seq = self.seq
            self.log.info(event)
            self.msg = self.single.single_identify_action(event)
            #self.log.info("in net_process_single_event")
            self.log.info(self.msg)
            #tornado.ioloop.IOLoop.instance().add_callback(self.send_message)
            self.send_message()
        except(socket.error, IOError, tornado.iostream.StreamClosedError, AssertionError):
            self.log.info("message Sender: Socket Connection closed from net_process_singe_event")
            self.disconnect_controller()
            self.sender.stop()


    def net_process_trigger_event(self):
        """Process trigger events"""
        if self.trigger is None:
            self.trigger = TriggerEvent()

    def net_process_message(self, hdr):
        """Process message and assign to appropriate function"""

        if hdr.type == E_TYPE_SINGLE:
            #self.log.info("Event type is Single EVENT")
            event = E_SINGLE.parse(self.__buffer[HEADER.sizeof():])
            self.log.info("Single event is "+str(event))

            #offset = HEADER.sizeof() + E_SINGLE.sizeof()
            self.net_process_single_event(event)


        elif hdr.type == E_TYPE_SCHED:
            #self.log.info("Event type is Scheduled EVENT")
            self.net_process_sched_event()

        elif hdr.type == E_TYPE_TRIG:
            #self.log.info("Event type is Trigger EVENT")
            self.net_process_trigger_event()

        else:
            self.log.error("Wrong event %u", hdr.type)


    def disconnect_controller(self):
        """Disconnect from controller"""

        self.stream.close()
        self.log.debug("Disconnected from controller")
        #tornado.ioloop.IOLoop.instance().add_timeout(2, self.connect_controller())
        #self.log.debug("Trying to connect after 2 seconds")



    def check_enbid(self):
        return self.enbid
########### RECEIVING ACTIONS###########
    def receive_message(self):

        return super().receive_message()


########### RECEIVING ACTIONS###########
