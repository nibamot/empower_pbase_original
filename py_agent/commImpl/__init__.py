#!/usr/bin/env python3
""" All basic pre requisites for agent operations"""
import socket
from socket import AF_INET, SOCK_STREAM


ADDRESS = 'localhost'
VALUE = 1
BUFF_SIZE = 4096
NET_CONNECTED = 0
